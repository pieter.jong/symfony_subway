<?php


namespace App\Services;


use App\Entity\Bread;
use App\Entity\Eater;
use App\Entity\Meal;
use App\Entity\Sandwich;
use App\RequestValidators\StoreSandwich;
use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class SandwichService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var StoreSandwich
     */
    private $storeSandwich;
    /**
     * @var Inflector
     */
    private $inflector;


    public function __construct(StoreSandwich $storeSandwich, EntityManagerInterface $em)
    {
        $this->inflector = InflectorFactory::create()->build();
        $this->storeSandwich = $storeSandwich;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @param Sandwich $sandwich
     * @param string $token
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Sandwich $sandwich, string $token, ?FormInterface $form = null)
    {
        $errors = $this->storeSandwich->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }

        /** @var Meal $meal */
        $meal = $this->em->getRepository(Meal::class)->findByToken($token);
        if (!$meal){
            return false;
        }

        /** @var Meal $meal */
        $meal = $this->em->getRepository(Meal::class)->findByToken($token);
        /** @var Eater $eater */
        $eater = $this->em->getRepository(Eater::class)->findByToken($token);

        $meal->addEater($eater);
        $this->em->persist($meal);

        $sandwich->setMeal($meal);
        $sandwich->setEater($eater);
        $this->em->persist($sandwich);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Bread $bread
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Bread $bread, FormInterface $form)
    {
        $errors = $this->storeSandwich->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($bread);
        $this->em->flush();

        return true;
    }

    public function combineSimultaneousSandwiches(Eater $eater): array
    {
        $array = [];
        /** @var Sandwich $sandwich */
        $sandWiches =$this->em->getRepository(Sandwich::class)->findByEater($eater);

        foreach ($sandWiches as $sandwich) {
            $arrayKey = sprintf(
                '%s%s%s%s%s%s%s',
                $sandwich->getBaked(),
                $sandwich->getBread()->getName(),
                $sandwich->getSauce()->getName(),
                $sandwich->getSize()->getSize(),
                $sandwich->getTaste()->getName(),
                $this->flattenManyRelations($sandwich->getExtras(), 'name'),
                $this->flattenManyRelations($sandwich->getVegetables(), 'name')
            );
            $array[$arrayKey][] = $sandwich;
        }

        $this->calculateRating($array);

        return $array;
    }

    private function flattenManyRelations($relation, $name, $separator = ','): string
    {
        $valuesArray = [];
        foreach ($relation as $item) {

            $valuesArray[] = $item->{'get' . $this->inflector->camelize($name)}();
        }
        return implode($separator, $valuesArray);
    }

    private function calculateRating(array &$array): void
    {
        foreach ($array as &$uniqueSandwich) {
            $rating = 0;
            /** @var Sandwich $item */
            foreach ($uniqueSandwich as $item) {
                $rating += $item->getRating();
            }
            $uniqueSandwich['timesEaten'] = count($uniqueSandwich);
            $uniqueSandwich['averageRating'] = $rating / $uniqueSandwich['timesEaten'];
        }
    }

    /**
     * @param Request $request
     * @param Sandwich $sandwich
     * @param string $token
     * @param FormInterface $form
     */
    public function rate(Request $request, Sandwich $sandwich, string $token, FormInterface $form)
    {
        $this->em->persist($sandwich);
        $this->em->flush();
    }
}