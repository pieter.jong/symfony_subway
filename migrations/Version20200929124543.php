<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200929124543 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_88270868A76ED395');
        $this->addSql('DROP INDEX IDX_88270868A76ED395 ON sandwich');
        $this->addSql('ALTER TABLE sandwich DROP user_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sandwich ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_88270868A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_88270868A76ED395 ON sandwich (user_id)');
    }
}
