# CodeGenerator

Well hi there! This repository holds the code and script
for the Symfony Subway. 

## Setup

It does'nt mather if you downloaded this repository from git or used git clone to fetch all the data.

Git is just mutch more easy once you now how to use it. Check here how u can install Git.

To get this project working, follow these steps:

1 - **Go to your folder where you just put all these files.**

2 - **Configure the the .env File**

First, make sure you have an `.env` file (you should).
If you don't, copy `.env.example` to create it.

Next, look at the configuration and make any adjustments you
need - specifically `DATABASE_URL`.

3 - **Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:**

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

4 - **Setup the Database**

Again, make sure `.env` is setup for your computer. Then, create
the database & tables!

```
php bin/console doctrine:database:create
php bin/console d:m:m
```
If you get an error "Could not find driver", make sure php can communicate with mysql by installing php-mysql:
- `sudo apt-get install php-mysql`

5 - **Load Fixtures**
```
doctrine:fixtures:load
```

6 - **Start the built-in web server**

You can use Nginx or Apache, but the built-in web server works
great:

```
symfony server:start
```

Now check out the site at `http://localhost:8000` or `http://127.0.0.1:8000` 

Have fun!


