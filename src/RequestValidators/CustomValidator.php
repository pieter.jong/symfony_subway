<?php

namespace App\RequestValidators;

use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class CustomValidator
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param Request $request
     * @param FormInterface|null $form
     * @return ConstraintViolationListInterface
     */
    public function validateObjectByRequest(
        Request $request,
        ?FormInterface $form = null
    ): ConstraintViolationListInterface {
        $objectVars = get_object_vars($this);

        unset($objectVars['validator']);
        foreach ($objectVars as $key => $property) {
            foreach ($request->request->all() as $requestItem) {
                $this->$key = $requestItem[$key];
            }
        }

        $errors = $this->validator->validate($this);

        if ($form !== null) {
            $this->addErrorsToForm($form, $errors);
        }

        return $errors;

    }

    /**
     * @param $form
     * @param ConstraintViolationListInterface $errors
     */
    private function addErrorsToForm(FormInterface $form, ConstraintViolationListInterface $errors): void
    {
        if (count($errors) > 0) {
            foreach ($errors as $violation) {
                $form->get($violation->getPropertyPath())->addError(new FormError($violation->getMessage()));
            }
        }
    }


}


