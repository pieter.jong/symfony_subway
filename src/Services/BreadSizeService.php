<?php


namespace App\Services;


use App\Entity\BreadSize;
use App\RequestValidators\StoreBreadSize;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class BreadSizeService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var BreadSize
     */
    private $storeBreadSize;


    public function __construct(StoreBreadSize $storeBreadSize, EntityManagerInterface $em, Security $security)
    {

        $this->storeBreadSize = $storeBreadSize;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param BreadSize $breadSize
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, BreadSize $breadSize, ?FormInterface $form = null)
    {
        $errors = $this->storeBreadSize->validateObjectByRequest($request, $form);
        if (count($errors)){
            return $errors;
        }
        $breadSize->setUser($this->security->getUser());
        $this->em->persist($breadSize);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param BreadSize $breadSize
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, BreadSize $breadSize, FormInterface $form)
    {
        $errors = $this->storeBreadSize->validateObjectByRequest($request, $form);
        if (count($errors)){
            return $errors;
        }
        $this->em->persist($breadSize);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param BreadSize $breadSize
     */
    public function delete(Request $request, BreadSize $breadSize): void
    {
        $this->em->remove($breadSize);
        $this->em->flush();
    }
}