<?php


namespace App\Services;


use App\Entity\Vegetables;
use App\RequestValidators\StoreVegetable;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class VegetablesService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var Vegetables
     */
    private $storeVegetable;


    public function __construct(StoreVegetable $storeVegetable, EntityManagerInterface $em, Security $security)
    {

        $this->storeVegetable = $storeVegetable;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Vegetables $vegetable
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Vegetables $vegetable, ?FormInterface $form = null)
    {
        $errors = $this->storeVegetable->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $vegetable->setUser($this->security->getUser());
        $this->em->persist($vegetable);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Vegetables $vegetable
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Vegetables $vegetable, FormInterface $form)
    {
        $errors = $this->storeVegetable->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($vegetable);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Vegetables $vegetable
     */
    public function delete(Request $request, Vegetables $vegetable): void
    {
        $this->em->remove($vegetable);
        $this->em->flush();
    }
}