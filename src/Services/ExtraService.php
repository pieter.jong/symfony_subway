<?php


namespace App\Services;


use App\Entity\Extra;
use App\RequestValidators\StoreExtra;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ExtraService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var Extra
     */
    private $storeExtra;


    public function __construct(StoreExtra $storeExtra, EntityManagerInterface $em, Security $security)
    {

        $this->storeExtra = $storeExtra;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Extra $extra
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Extra $extra, ?FormInterface $form = null)
    {
//        $errors = $this->storeExtra->validateObjectByRequest($request, $form);
//        if (count($errors)) {
//            return $errors;
//        }
        $extra->setUser($this->security->getUser());
        $this->em->persist($extra);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Extra $extra
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Extra $extra, FormInterface $form)
    {
        $errors = $this->storeExtra->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($extra);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Extra $extra
     */
    public function delete(Request $request, Extra $extra): void
    {
        $this->em->remove($extra);
        $this->em->flush();
    }
}