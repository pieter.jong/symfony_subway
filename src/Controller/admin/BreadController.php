<?php

namespace App\Controller\admin;

use App\Entity\Bread;
use App\Form\BreadType;
use App\Services\BreadService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bread")
 * @IsGranted("ROLE_ADMIN_BREAD");
 */
class BreadController extends AbstractController
{

    /**
     * @var BreadService
     */
    private $breadService;

    public function __construct(BreadService $breadService)
    {
        $this->breadService = $breadService;
    }

    /**
     * @Route("/new", name="bread_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $bread = new Bread();
        $form = $this->createForm(BreadType::class, $bread);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->breadService->store($request, $bread, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/bread/new.html.twig', [
            'bread' => $bread,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bread_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Bread $bread
     * @return Response
     */
    public function edit(Request $request, Bread $bread): Response
    {
        $form = $this->createForm(BreadType::class, $bread);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->breadService->update($request, $bread, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/bread/edit.html.twig', [
            'bread' => $bread,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bread_delete", methods={"DELETE"})
     * @param Request $request
     * @param Bread $bread
     * @return Response
     */
    public function delete(Request $request, Bread $bread): Response
    {
        $this->breadService->delete($request, $bread);

        return $this->redirectToRoute('sandwich_settings');
    }
}
