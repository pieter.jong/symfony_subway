<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200927165224 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bread (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_9CC34D4BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bread_size (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, size INT NOT NULL, INDEX IDX_A2A550C6A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE eater (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_E113D038A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE extra (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, price_inc DOUBLE PRECISION NOT NULL, price_ex DOUBLE PRECISION NOT NULL, INDEX IDX_4D3F0D65A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meal (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, is_open TINYINT(1) NOT NULL, date_time DATETIME NOT NULL, INDEX IDX_9EF68E9CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meal_eater (meal_id INT NOT NULL, eater_id INT NOT NULL, INDEX IDX_5BD938EE639666D6 (meal_id), INDEX IDX_5BD938EE49BFF538 (eater_id), PRIMARY KEY(meal_id, eater_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE meal_token (id INT AUTO_INCREMENT NOT NULL, eater_id INT NOT NULL, meal_id INT NOT NULL, token VARCHAR(255) NOT NULL, is_active TINYINT(1) NOT NULL, INDEX IDX_E5FD49ED49BFF538 (eater_id), INDEX IDX_E5FD49ED639666D6 (meal_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sandwich (id INT AUTO_INCREMENT NOT NULL, bread_id INT NOT NULL, size_id INT NOT NULL, taste_id INT NOT NULL, sauce_id INT NOT NULL, user_id INT NOT NULL, meal_id INT DEFAULT NULL, eater_id INT NOT NULL, baked TINYINT(1) NOT NULL, INDEX IDX_882708685D3405CF (bread_id), INDEX IDX_88270868498DA827 (size_id), INDEX IDX_8827086874E52521 (taste_id), INDEX IDX_882708687AB984B7 (sauce_id), INDEX IDX_88270868A76ED395 (user_id), INDEX IDX_88270868639666D6 (meal_id), INDEX IDX_8827086849BFF538 (eater_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sandwich_extra (sandwich_id INT NOT NULL, extra_id INT NOT NULL, INDEX IDX_C113FAB04D566043 (sandwich_id), INDEX IDX_C113FAB02B959FC6 (extra_id), PRIMARY KEY(sandwich_id, extra_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sandwich_vegetables (sandwich_id INT NOT NULL, vegetables_id INT NOT NULL, INDEX IDX_C4C986D44D566043 (sandwich_id), INDEX IDX_C4C986D488FCF930 (vegetables_id), PRIMARY KEY(sandwich_id, vegetables_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sauce (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_DAF8FA6CA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE taste (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_69D6DE58A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vegetables (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_380C3020A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bread ADD CONSTRAINT FK_9CC34D4BA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE bread_size ADD CONSTRAINT FK_A2A550C6A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE eater ADD CONSTRAINT FK_E113D038A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE extra ADD CONSTRAINT FK_4D3F0D65A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE meal ADD CONSTRAINT FK_9EF68E9CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE meal_eater ADD CONSTRAINT FK_5BD938EE639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meal_eater ADD CONSTRAINT FK_5BD938EE49BFF538 FOREIGN KEY (eater_id) REFERENCES eater (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE meal_token ADD CONSTRAINT FK_E5FD49ED49BFF538 FOREIGN KEY (eater_id) REFERENCES eater (id)');
        $this->addSql('ALTER TABLE meal_token ADD CONSTRAINT FK_E5FD49ED639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_882708685D3405CF FOREIGN KEY (bread_id) REFERENCES bread (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_88270868498DA827 FOREIGN KEY (size_id) REFERENCES bread_size (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_8827086874E52521 FOREIGN KEY (taste_id) REFERENCES taste (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_882708687AB984B7 FOREIGN KEY (sauce_id) REFERENCES sauce (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_88270868A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_88270868639666D6 FOREIGN KEY (meal_id) REFERENCES meal (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_8827086849BFF538 FOREIGN KEY (eater_id) REFERENCES eater (id)');
        $this->addSql('ALTER TABLE sandwich_extra ADD CONSTRAINT FK_C113FAB04D566043 FOREIGN KEY (sandwich_id) REFERENCES sandwich (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sandwich_extra ADD CONSTRAINT FK_C113FAB02B959FC6 FOREIGN KEY (extra_id) REFERENCES extra (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sandwich_vegetables ADD CONSTRAINT FK_C4C986D44D566043 FOREIGN KEY (sandwich_id) REFERENCES sandwich (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sandwich_vegetables ADD CONSTRAINT FK_C4C986D488FCF930 FOREIGN KEY (vegetables_id) REFERENCES vegetables (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sauce ADD CONSTRAINT FK_DAF8FA6CA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE taste ADD CONSTRAINT FK_69D6DE58A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vegetables ADD CONSTRAINT FK_380C3020A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_882708685D3405CF');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_88270868498DA827');
        $this->addSql('ALTER TABLE meal_eater DROP FOREIGN KEY FK_5BD938EE49BFF538');
        $this->addSql('ALTER TABLE meal_token DROP FOREIGN KEY FK_E5FD49ED49BFF538');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_8827086849BFF538');
        $this->addSql('ALTER TABLE sandwich_extra DROP FOREIGN KEY FK_C113FAB02B959FC6');
        $this->addSql('ALTER TABLE meal_eater DROP FOREIGN KEY FK_5BD938EE639666D6');
        $this->addSql('ALTER TABLE meal_token DROP FOREIGN KEY FK_E5FD49ED639666D6');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_88270868639666D6');
        $this->addSql('ALTER TABLE sandwich_extra DROP FOREIGN KEY FK_C113FAB04D566043');
        $this->addSql('ALTER TABLE sandwich_vegetables DROP FOREIGN KEY FK_C4C986D44D566043');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_882708687AB984B7');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_8827086874E52521');
        $this->addSql('ALTER TABLE bread DROP FOREIGN KEY FK_9CC34D4BA76ED395');
        $this->addSql('ALTER TABLE bread_size DROP FOREIGN KEY FK_A2A550C6A76ED395');
        $this->addSql('ALTER TABLE eater DROP FOREIGN KEY FK_E113D038A76ED395');
        $this->addSql('ALTER TABLE extra DROP FOREIGN KEY FK_4D3F0D65A76ED395');
        $this->addSql('ALTER TABLE meal DROP FOREIGN KEY FK_9EF68E9CA76ED395');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_88270868A76ED395');
        $this->addSql('ALTER TABLE sauce DROP FOREIGN KEY FK_DAF8FA6CA76ED395');
        $this->addSql('ALTER TABLE taste DROP FOREIGN KEY FK_69D6DE58A76ED395');
        $this->addSql('ALTER TABLE vegetables DROP FOREIGN KEY FK_380C3020A76ED395');
        $this->addSql('ALTER TABLE sandwich_vegetables DROP FOREIGN KEY FK_C4C986D488FCF930');
        $this->addSql('DROP TABLE bread');
        $this->addSql('DROP TABLE bread_size');
        $this->addSql('DROP TABLE eater');
        $this->addSql('DROP TABLE extra');
        $this->addSql('DROP TABLE meal');
        $this->addSql('DROP TABLE meal_eater');
        $this->addSql('DROP TABLE meal_token');
        $this->addSql('DROP TABLE sandwich');
        $this->addSql('DROP TABLE sandwich_extra');
        $this->addSql('DROP TABLE sandwich_vegetables');
        $this->addSql('DROP TABLE sauce');
        $this->addSql('DROP TABLE taste');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vegetables');
    }
}
