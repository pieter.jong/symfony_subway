<?php


namespace App\Services;


use App\Entity\Bread;
use App\RequestValidators\StoreEater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class BreadService
{
    /**
     * @var StoreEater
     */
    private $storeBread;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;


    public function __construct(StoreEater $storeBread, EntityManagerInterface $em, Security $security)
    {

        $this->storeBread = $storeBread;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Bread $bread
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Bread $bread, ?FormInterface $form = null)
    {
        $errors = $this->storeBread->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $bread->setUser($this->security->getUser());
        $this->em->persist($bread);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Bread $bread
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Bread $bread, FormInterface $form)
    {
        $errors = $this->storeBread->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($bread);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Bread $bread
     */
    public function delete(Request $request, Bread $bread): void
    {
        $this->em->remove($bread);
        $this->em->flush();
    }
}