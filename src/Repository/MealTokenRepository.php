<?php

namespace App\Repository;

use App\Entity\MealToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MealToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method MealToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method MealToken[]    findAll()
 * @method MealToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MealToken::class);
    }

    public function findByMealIndexedByEater($meal): array
    {
        $results =  $this->createQueryBuilder('mt')
            ->andWhere('mt.meal = :meal')
            ->setParameter('meal', $meal)
            ->getQuery()
            ->getResult();
        $array = [];

        /** @var MealToken $item */
        foreach ($results as $item) {
            $array[$item->getEater()->getId()] = $item->getToken();
        }
        return $array;
    }

}
