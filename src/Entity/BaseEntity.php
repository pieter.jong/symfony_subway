<?php


namespace App\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class BaseEntity
 * @package App\Entity
 */
abstract class BaseEntity
{

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $updatedAt;


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $dateTimeNow = new DateTime('now');
        $this->setUpdatedAt($dateTimeNow);
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($dateTimeNow);
        }
    }


    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return BaseEntity
     */
    public function setCreatedAt(DateTime $createdAt): BaseEntity
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     * @return BaseEntity
     */
    public function setUpdatedAt(DateTime $updatedAt): BaseEntity
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

}

