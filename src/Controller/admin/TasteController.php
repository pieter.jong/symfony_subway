<?php

namespace App\Controller\admin;

use App\Entity\Taste;
use App\Form\TasteType;
use App\Services\TasteService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/taste")
 * @IsGranted("ROLE_ADMIN_TASTE");
 */
class TasteController extends AbstractController
{

    /**
     * @var TasteService
     */
    private $tasteService;

    public function __construct(TasteService $tasteService)
    {
        $this->tasteService = $tasteService;
    }

    /**
     * @Route("/new", name="taste_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $taste = new Taste();
        $form = $this->createForm(TasteType::class, $taste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->tasteService->store($request, $taste, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/taste/new.html.twig', [
            'taste' => $taste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="taste_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Taste $taste
     * @return Response
     */
    public function edit(Request $request, Taste $taste): Response
    {
        $form = $this->createForm(TasteType::class, $taste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->tasteService->update($request, $taste, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/taste/edit.html.twig', [
            'taste' => $taste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="taste_delete", methods={"DELETE"})
     * @param Request $request
     * @param Taste $taste
     * @return Response
     */
    public function delete(Request $request, Taste $taste): Response
    {
        $this->tasteService->delete($request, $taste);

        return $this->redirectToRoute('sandwich_settings');
    }
}
