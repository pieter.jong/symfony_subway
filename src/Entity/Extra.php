<?php

namespace App\Entity;

use App\Repository\ExtraRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ExtraRepository::class)
 */
class Extra
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="float")
     * @Assert\Type("float")
     */
    private $priceInc;

    /**
     * @ORM\Column(type="float", length=255)
     * @Assert\Type("float")
     */
    private $priceEx;

    /**
     * @ORM\ManyToMany(targetEntity=Sandwich::class, mappedBy="extras")
     */
    private $sandwiches;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="extras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPriceInc(): ?float
    {
        return (float)$this->priceInc;
    }

    public function setPriceInc(float $priceInc): self
    {
        $this->priceInc = $priceInc;

        return $this;
    }

    public function getPriceEx(): ?float
    {
        return $this->priceEx;
    }

    public function setPriceEx(float $priceEx): self
    {
        $this->priceEx = $priceEx;

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->addExtra($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            $sandwich->removeExtra($this);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
