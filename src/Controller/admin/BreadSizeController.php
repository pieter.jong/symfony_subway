<?php

namespace App\Controller\admin;

use App\Entity\BreadSize;
use App\Form\BreadSizeType;
use App\Services\BreadSizeService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bread/size")
 * @IsGranted("ROLE_ADMIN_BREAD_SIZE");
 */
class BreadSizeController extends AbstractController
{

    /**
     * @var BreadSizeService
     */
    private $breadSizeService;

    public function __construct(BreadSizeService $breadSizeService)
    {
        $this->breadSizeService = $breadSizeService;
    }

    /**
     * @Route("/new", name="bread_size_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $breadSize = new BreadSize();
        $form = $this->createForm(BreadSizeType::class, $breadSize);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->breadSizeService->store($request, $breadSize, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/bread_size/new.html.twig', [
            'bread_size' => $breadSize,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bread_size_edit", methods={"GET","POST"})
     * @param Request $request
     * @param BreadSize $breadSize
     * @return Response
     */
    public function edit(Request $request, BreadSize $breadSize): Response
    {
        $form = $this->createForm(BreadSizeType::class, $breadSize);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->breadSizeService->update($request, $breadSize, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/bread_size/edit.html.twig', [
            'bread_size' => $breadSize,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bread_size_delete", methods={"DELETE"})
     * @param Request $request
     * @param BreadSize $breadSize
     * @return Response
     */
    public function delete(Request $request, BreadSize $breadSize): Response
    {
        $this->breadSizeService->delete($request, $breadSize);

        return $this->redirectToRoute('sandwich_settings');
    }
}
