<?php

namespace App\Repository;

use App\Entity\Eater;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Eater|null find($id, $lockMode = null, $lockVersion = null)
 * @method Eater|null findOneBy(array $criteria, array $orderBy = null)
 * @method Eater[]    findAll()
 * @method Eater[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EaterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Eater::class);
    }



    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('e.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $token
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function getEaterIfValidToken(string $token)
    {
        return $this->createQueryBuilder('e')
            ->join('e.mealTokens', 'mealTokens')
            ->andWhere('mealTokens.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $token
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function findByToken(string $token)
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.mealTokens', 'tokens')
            ->andWhere('tokens.token = :token')
            ->setParameter('token', $token)
            ->andWhere('tokens.isActive = true')
            ->getQuery()
            ->getOneOrNullResult();
    }
}
