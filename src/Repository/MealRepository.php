<?php

namespace App\Repository;

use App\Entity\Meal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Meal|null find($id, $lockMode = null, $lockVersion = null)
 * @method Meal|null findOneBy(array $criteria, array $orderBy = null)
 * @method Meal[]    findAll()
 * @method Meal[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MealRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Meal::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.user = :admin')
            ->setParameter('admin', $admin)
            ->andWhere('m.isOpen = false')
            ->getQuery()
            ->getResult();
    }

    public function CurrentMealByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.user = :admin')
            ->andWhere('m.isOpen = true')
            ->setParameter('admin', $admin)
            ->getQuery()
            ->getOneOrNullResult();

    }

    public function findByToken(string $token)
    {
        return $this->createQueryBuilder('m')
            ->leftJoin('m.tokens', 'tokens')
            ->andWhere('tokens.token = :token')
            ->setParameter('token', $token)
            ->andWhere('m.isOpen = true')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function hasOpenMeal(?UserInterface $user): bool
    {
        $count = $this->createQueryBuilder('m')
            ->select('COUNT(m)')
            ->andWhere('m.user = :user')
            ->setParameter('user', $user)
            ->andWhere('m.isOpen = true')
            ->getQuery()
            ->getSingleScalarResult();

        return $count > 0;
    }
}
