<?php

namespace App\Controller\admin;

use App\Entity\Extra;
use App\Form\ExtraType;
use App\Services\ExtraService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/extra")
 * @IsGranted("ROLE_ADMIN_EXTRA");
 */
class ExtraController extends AbstractController
{
    /**
     * @var ExtraService
     */
    private $extraService;

    public function __construct(ExtraService $extraService)
    {
        $this->extraService = $extraService;
    }

    /**
     * @Route("/new", name="extra_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $extra = new Extra();
        $form = $this->createForm(ExtraType::class, $extra);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->extraService->store($request, $extra);
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/extra/new.html.twig', [
            'extra' => $extra,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="extra_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Extra $extra
     * @return Response
     */
    public function edit(Request $request, Extra $extra): Response
    {
        $form = $this->createForm(ExtraType::class, $extra);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->extraService->update($request, $extra, $form);
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/extra/edit.html.twig', [
            'extra' => $extra,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="extra_delete", methods={"DELETE"})
     * @param Request $request
     * @param Extra $extra
     * @return Response
     */
    public function delete(Request $request, Extra $extra): Response
    {
        $this->extraService->delete($request, $extra);

        return $this->redirectToRoute('sandwich_settings');
    }
}
