<?php

namespace App\Repository;

use App\Entity\Eater;
use App\Entity\Sandwich;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Sandwich|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sandwich|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sandwich[]    findAll()
 * @method Sandwich[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SandwitchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sandwich::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('s.', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Eater $eater
     * @param $meal
     * @return int|mixed|string
     */
    public function findByEaterAndMeal(Eater $eater, $meal)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.meal = :meal')
            ->setParameter('meal', $meal)
            ->andWhere('s.eater = :eater')
            ->setParameter('eater', $eater)
            ->getQuery()
            ->getResult();
    }

    public function findByEater(Eater $eater)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.eater = :eater')
            ->setParameter('eater', $eater)
            ->leftJoin('s.bread', 'bread')
            ->leftJoin('s.size', 'breadSize')
            ->leftJoin('s.taste', 'taste')
            ->leftJoin('s.sauce', 'sauce')
            ->leftJoin('s.vegetables', 'vegetables')
            ->leftJoin('s.extras', 'extras')
            ->addSelect('bread')
            ->addSelect('breadSize')
            ->addSelect('taste')
            ->addSelect('sauce')
            ->addSelect('vegetables')
            ->addSelect('extras')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Eater $eater
     * @param Sandwich $sandwich
     * @return bool
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function eaterBelongsToSandwich(Eater $eater, Sandwich $sandwich): bool
    {
        $count = $this->createQueryBuilder('s')
            ->select('COUNT(s)')
            ->andWhere('s = :sandwich')
            ->setParameter('sandwich', $sandwich)
            ->andWhere('s.eater = :eater')
            ->setParameter('eater', $eater)
            ->getQuery()
            ->getSingleScalarResult();

        return $count > 0;
    }
}
