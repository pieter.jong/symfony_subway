<?php

namespace App\Entity;

use App\Repository\MealRepository;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MealRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Meal extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="meals")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isOpen = true;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTime;


    /**
     * @ORM\OneToMany(targetEntity=MealToken::class, mappedBy="meal", orphanRemoval=true)
     */
    private $tokens;

    /**
     * @ORM\ManyToMany(targetEntity=Eater::class, inversedBy="meals")
     */
    private $eaters;

    /**
     * @ORM\OneToMany(targetEntity=Sandwich::class, mappedBy="meal")
     */
    private $sandwiches;

    /**
     * @ORM\OneToOne(targetEntity=Weather::class, mappedBy="meal", cascade={"persist", "remove"})
     */
    private $weather;

    public function __construct()
    {
        $this->tokens = new ArrayCollection();
        $this->eaters = new ArrayCollection();
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getIsOpen(): ?bool
    {
        return $this->isOpen;
    }

    public function setIsOpen(bool $isOpen): self
    {
        $this->isOpen = $isOpen;

        return $this;
    }

    public function getDateTime(): ?DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }


    /**
     * @return Collection|MealToken[]
     */
    public function getTokens(): Collection
    {
        return $this->tokens;
    }

    public function addToken(MealToken $token): self
    {
        if (!$this->tokens->contains($token)) {
            $this->tokens[] = $token;
            $token->setMeal($this);
        }

        return $this;
    }

    public function removeToken(MealToken $token): self
    {
        if ($this->tokens->contains($token)) {
            $this->tokens->removeElement($token);
            // set the owning side to null (unless already changed)
            if ($token->getMeal() === $this) {
                $token->setMeal(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Eater[]
     */
    public function getEaters(): Collection
    {
        return $this->eaters;
    }

    public function addEater(Eater $eater): self
    {
        if (!$this->eaters->contains($eater)) {
            $this->eaters[] = $eater;
        }

        return $this;
    }

    public function removeEater(Eater $eater): self
    {
        if ($this->eaters->contains($eater)) {
            $this->eaters->removeElement($eater);
        }

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->setMeal($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            // set the owning side to null (unless already changed)
            if ($sandwich->getMeal() === $this) {
                $sandwich->setMeal(null);
            }
        }

        return $this;
    }

    public function getWeather(): ?Weather
    {
        return $this->weather;
    }

    public function setWeather(?Weather $weather): self
    {
        $this->weather = $weather;

        // set (or unset) the owning side of the relation if necessary
        $newMeal = null === $weather ? null : $this;
        if ($weather->getMeal() !== $newMeal) {
            $weather->setMeal($newMeal);
        }

        return $this;
    }
}
