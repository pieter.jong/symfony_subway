<?php


namespace App\Services;


use App\Entity\Weather;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class WeatherService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function findWeatherByDateTime(DateTime $dateTime)
    {
        $weather = $this->em->getRepository(Weather::class)->getClosestWeatherByDateTime($dateTime);

        if (!$weather){
            return null;
        }

        return $weather;
    }
}