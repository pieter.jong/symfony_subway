<?php


namespace App\Services;


use App\Entity\Bread;
use App\Entity\Eater;
use App\Entity\Meal;
use App\Entity\MealToken;
use App\RequestValidators\StoreEater;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class MealService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserInterface|null
     */
    private $user;
    /**
     * @var WeatherService
     */
    private $weatherService;
    /**
     * @var MailerInterface
     */
    private $mailer;


    public function __construct(
        StoreEater $storeBread,
        EntityManagerInterface $em,
        Security $security,
        WeatherService $weatherService,
        MailerInterface $mailer
    ) {

        $this->storeBread = $storeBread;
        $this->em = $em;

        $this->user = $security->getUser();
        $this->weatherService = $weatherService;
        $this->mailer = $mailer;
    }

    /**
     * @return void
     * @throws TransportExceptionInterface
     */
    public function open(): void
    {
        if ($this->hasOpenMeals()){
            return;
        }
        $meal = new Meal();
        $meal->setDateTime((new DateTime()));
        $meal->setUser($this->user);
        $meal->setIsOpen(true);
        $weather = $this->weatherService->findWeatherByDateTime($meal->getDateTime());
        if ($weather) {
            $meal->setWeather($weather);
        }
        $this->em->persist($meal);
        $this->em->flush();
        $this->handleTokensForMeal($meal);
        $this->sendInviteByMail($meal);
    }

    /**
     * @param Request $request
     * @param Bread $bread
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Bread $bread, FormInterface $form)
    {
        $errors = $this->storeBread->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($bread);
        $this->em->flush();

        return true;
    }

    /**
     * @param Meal $meal
     * @throws Exception
     */
    private function handleTokensForMeal(Meal $meal): void
    {
        /** @var Eater $eater */
        foreach ($this->user->getEaters() as $eater) {
            $md5 = hash('md5', $eater->getName() . $meal->getDateTime()->format('dmYhis') . random_bytes(5));
            $token = new MealToken();
            $token->setEater($eater);
            $token->setIsActive(true);
            $token->setMeal($meal);
            $token->setToken($md5);
            $this->em->persist($token);
            $this->em->flush();

            $eater->addMealToken($token);
            $this->em->persist($eater);

            $meal->addToken($token);
        }
    }

    /**
     * @param Meal $meal
     */
    public function close(Meal $meal): void
    {
        $meal->setIsOpen(false);
        $this->em->persist($meal);
        $this->em->flush();
    }

    /**
     * @param Meal $meal
     * @throws TransportExceptionInterface
     */
    private function sendInviteByMail(Meal $meal): void
    {
        $tokensIndexedByEater = $this->em->getRepository(MealToken::class)->findByMealIndexedByEater($meal);
        $eaters = $this->em->getRepository(Eater::class)->findByAdmin($meal->getUser());
        /** @var Eater $eater */
        foreach ($eaters as $eater) {
            if (!array_key_exists($eater->getId(), $tokensIndexedByEater)){
                continue;
            }

            $email = new Email();
            $email->from($_ENV['FROM__EMAIL_ADDRESS'])
                ->to($eater->getEmail())
                ->subject('Time to compose your sandwich')
                ->text(sprintf(
                    'Hello %s,
                
Time has come to creat you subway sandwich or maybe even more than one sandwich.
You can composer your sandwich for this meal at https://127.0.0.1:8000/sandwich/%s',
                    $eater->getName(),
                    $tokensIndexedByEater[$eater->getId()]
                ));
            dump($email);
            $this->mailer->send($email);
        }
    }

    private function hasOpenMeals(): bool
    {
        return $this->em->getRepository(Meal::class)->hasOpenMeal($this->user);
    }
}