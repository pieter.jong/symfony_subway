<?php


namespace App\Services;


use App\Entity\Bread;
use App\Entity\Eater;
use App\RequestValidators\StoreEater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class EaterService
{
    /**
     * @var StoreEater
     */
    private $storeEater;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;


    public function __construct(StoreEater $storeEater, EntityManagerInterface $em, Security $security)
    {

        $this->storeEater = $storeEater;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Eater $eater
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Eater $eater, ?FormInterface $form = null)
    {
        $errors = $this->storeEater->validateObjectByRequest($request, $form);
        if (count($errors)){
            return $errors;
        }
        $eater->setUser($this->security->getUser());
        $this->em->persist($eater);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Bread $bread
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Bread $bread, FormInterface $form)
    {
        $errors = $this->storeEater->validateObjectByRequest($request, $form);
        if (count($errors)){
            return $errors;
        }
        $this->em->persist($bread);
        $this->em->flush();

        return true;
    }
}