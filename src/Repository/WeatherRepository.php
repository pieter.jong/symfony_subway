<?php

namespace App\Repository;

use App\Entity\Weather;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Weather|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weather|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weather[]    findAll()
 * @method Weather[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeatherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weather::class);
    }

    /**
     * @param DateTime $dateTime
     * @return int|mixed|string|null
     * @throws NonUniqueResultException
     */
    public function getClosestWeatherByDateTime(DateTime $dateTime)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.createdAt < :dateTime')
            ->setParameter('dateTime', $dateTime)
            ->addOrderBy('w.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
