<?php

namespace App\Form;

use App\Entity\Bread;
use App\Entity\BreadSize;
use App\Entity\Extra;
use App\Entity\Sandwich;
use App\Entity\Sauce;
use App\Entity\Taste;
use App\Entity\Vegetables;
use App\Repository\BreadRepository;
use App\Repository\BreadSizeRepository;
use App\Repository\ExtraRepository;
use App\Repository\SauceRepository;
use App\Repository\TasteRepository;
use App\Repository\VegetablesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SandwichType extends AbstractType
{
    /**
     * @var BreadRepository
     */
    private $breadRepository;
    /**
     * @var BreadSizeRepository
     */
    private $breadSizeRepository;
    /**
     * @var TasteRepository
     */
    private $tasteRepository;
    /**
     * @var ExtraRepository
     */
    private $extraRepository;
    /**
     * @var VegetablesRepository
     */
    private $vegetablesRepository;
    /**
     * @var SauceRepository
     */
    private $sauceRepository;

    public function __construct(
        BreadRepository $breadRepository,
        BreadSizeRepository $breadSizeRepository,
        TasteRepository $tasteRepository,
        ExtraRepository $extraRepository,
        VegetablesRepository $vegetablesRepository,
        SauceRepository $sauceRepository
    ) {

        $this->breadRepository = $breadRepository;
        $this->breadSizeRepository = $breadSizeRepository;
        $this->tasteRepository = $tasteRepository;
        $this->extraRepository = $extraRepository;
        $this->vegetablesRepository = $vegetablesRepository;
        $this->sauceRepository = $sauceRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('baked')
            ->add('bread', EntityType::class, [
                'class' => Bread::class,
                'choice_label' => 'name',
                'choices' => $this->breadRepository->findByAdmin($options['admin']),
                'label' => ''
            ])
            ->add('size', EntityType::class, [
                'class' => BreadSize::class,
                'choice_label' => 'size',
                'choices' => $this->breadSizeRepository->findByAdmin($options['admin']),
                'label' => ''
            ])
            ->add('taste', EntityType::class, [
                'class' => Taste::class,
                'choice_label' => 'name',
                'choices' => $this->tasteRepository->findByAdmin($options['admin']),
                'label' => ''
            ])
            ->add('extras', EntityType::class, [
                'class' => Extra::class,
                'choice_label' => 'name',
                'multiple' => true,
                'choices' => $this->extraRepository->findByAdmin($options['admin']),
                'label' => ''
            ])
            ->add('vegetables', EntityType::class, [
                'class' => Vegetables::class,
                'choice_label' => 'name',
                'multiple' => true,
                'choices' => $this->vegetablesRepository->findByAdmin($options['admin']),
                'label' => ''
            ])
            ->add('sauce', EntityType::class, [
                'class' => Sauce::class,
                'choice_label' => 'name',
                'choices' => $this->sauceRepository->findByAdmin($options['admin']),
                'label' => ''
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sandwich::class,
            'admin' => null
        ]);
    }
}
