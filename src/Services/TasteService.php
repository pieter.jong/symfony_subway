<?php


namespace App\Services;


use App\Entity\Taste;
use App\RequestValidators\StoreEater;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class TasteService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var Taste
     */
    private $storeTaste;


    public function __construct(StoreEater $storeTaste, EntityManagerInterface $em, Security $security)
    {

        $this->storeTaste = $storeTaste;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Taste $taste
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Taste $taste, ?FormInterface $form = null)
    {
        $errors = $this->storeTaste->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $taste->setUser($this->security->getUser());
        $this->em->persist($taste);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Taste $taste
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Taste $taste, FormInterface $form)
    {
        $errors = $this->storeTaste->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($taste);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Taste $taste
     */
    public function delete(Request $request, Taste $taste): void
    {
        $this->em->remove($taste);
        $this->em->flush();
    }
}