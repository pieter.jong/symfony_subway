<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Bread::class, mappedBy="user")
     */
    private $breads;

    /**
     * @ORM\OneToMany(targetEntity=BreadSize::class, mappedBy="extras")
     */
    private $breadSizes;

    /**
     * @ORM\OneToMany(targetEntity=Extra::class, mappedBy="user")
     */
    private $extras;

    /**
     * @ORM\OneToMany(targetEntity=Meal::class, mappedBy="user")
     */
    private $meals;

    /**
     * @ORM\OneToMany(targetEntity=Sauce::class, mappedBy="user")
     */
    private $sauces;

    /**
     * @ORM\OneToMany(targetEntity=Taste::class, mappedBy="user")
     */
    private $tastes;

    /**
     * @ORM\OneToMany(targetEntity=Vegetables::class, mappedBy="user")
     */
    private $vegetables;

    /**
     * @ORM\OneToMany(targetEntity=Eater::class, mappedBy="user")
     */
    private $eaters;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isVerified = false;

    public function __construct()
    {
        $this->breads = new ArrayCollection();
        $this->breadSizes = new ArrayCollection();
        $this->extras = new ArrayCollection();
        $this->meals = new ArrayCollection();
        $this->sauces = new ArrayCollection();
        $this->tastes = new ArrayCollection();
        $this->vegetables = new ArrayCollection();
        $this->eaters = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Bread[]
     */
    public function getBreads(): Collection
    {
        return $this->breads;
    }

    public function addBread(Bread $bread): self
    {
        if (!$this->breads->contains($bread)) {
            $this->breads[] = $bread;
            $bread->setUser($this);
        }

        return $this;
    }

    public function removeBread(Bread $bread): self
    {
        if ($this->breads->contains($bread)) {
            $this->breads->removeElement($bread);
            // set the owning side to null (unless already changed)
            if ($bread->getUser() === $this) {
                $bread->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BreadSize[]
     */
    public function getBreadSizes(): Collection
    {
        return $this->breadSizes;
    }

    public function addBreadSize(BreadSize $breadSize): self
    {
        if (!$this->breadSizes->contains($breadSize)) {
            $this->breadSizes[] = $breadSize;
            $breadSize->setUser($this);
        }

        return $this;
    }

    public function removeBreadSize(BreadSize $breadSize): self
    {
        if ($this->breadSizes->contains($breadSize)) {
            $this->breadSizes->removeElement($breadSize);
            // set the owning side to null (unless already changed)
            if ($breadSize->getUser() === $this) {
                $breadSize->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Extra[]
     */
    public function getExtras(): Collection
    {
        return $this->extras;
    }

    public function addExtra(Extra $extra): self
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
            $extra->setUser($this);
        }

        return $this;
    }

    public function removeExtra(Extra $extra): self
    {
        if ($this->extras->contains($extra)) {
            $this->extras->removeElement($extra);
            // set the owning side to null (unless already changed)
            if ($extra->getUser() === $this) {
                $extra->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Meal[]
     */
    public function getMeals(): Collection
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal): self
    {
        if (!$this->meals->contains($meal)) {
            $this->meals[] = $meal;
            $meal->setUser($this);
        }

        return $this;
    }

    public function removeMeal(Meal $meal): self
    {
        if ($this->meals->contains($meal)) {
            $this->meals->removeElement($meal);
            // set the owning side to null (unless already changed)
            if ($meal->getUser() === $this) {
                $meal->setUser(null);
            }
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            // set the owning side to null (unless already changed)
            if ($sandwich->getUser() === $this) {
                $sandwich->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Sauce[]
     */
    public function getSauces(): Collection
    {
        return $this->sauces;
    }

    public function addSauce(Sauce $sauce): self
    {
        if (!$this->sauces->contains($sauce)) {
            $this->sauces[] = $sauce;
            $sauce->setUser($this);
        }

        return $this;
    }

    public function removeSauce(Sauce $sauce): self
    {
        if ($this->sauces->contains($sauce)) {
            $this->sauces->removeElement($sauce);
            // set the owning side to null (unless already changed)
            if ($sauce->getUser() === $this) {
                $sauce->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Taste[]
     */
    public function getTastes(): Collection
    {
        return $this->tastes;
    }

    public function addTaste(Taste $taste): self
    {
        if (!$this->tastes->contains($taste)) {
            $this->tastes[] = $taste;
            $taste->setUser($this);
        }

        return $this;
    }

    public function removeTaste(Taste $taste): self
    {
        if ($this->tastes->contains($taste)) {
            $this->tastes->removeElement($taste);
            // set the owning side to null (unless already changed)
            if ($taste->getUser() === $this) {
                $taste->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Vegetables[]
     */
    public function getVegetables(): Collection
    {
        return $this->vegetables;
    }

    public function addVegetable(Vegetables $vegetable): self
    {
        if (!$this->vegetables->contains($vegetable)) {
            $this->vegetables[] = $vegetable;
            $vegetable->setUser($this);
        }

        return $this;
    }

    public function removeVegetable(Vegetables $vegetable): self
    {
        if ($this->vegetables->contains($vegetable)) {
            $this->vegetables->removeElement($vegetable);
            // set the owning side to null (unless already changed)
            if ($vegetable->getUser() === $this) {
                $vegetable->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Eater[]
     */
    public function getEaters(): Collection
    {
        return $this->eaters;
    }

    public function addEater(Eater $eater): self
    {
        if (!$this->eaters->contains($eater)) {
            $this->eaters[] = $eater;
            $eater->setUser($this);
        }

        return $this;
    }

    public function removeEater(Eater $eater): self
    {
        if ($this->eaters->contains($eater)) {
            $this->eaters->removeElement($eater);
            // set the owning side to null (unless already changed)
            if ($eater->getUser() === $this) {
                $eater->setUser(null);
            }
        }

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }
}
