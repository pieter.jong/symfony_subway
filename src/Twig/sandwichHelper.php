<?php


namespace App\Twig;


use Doctrine\Inflector\InflectorFactory;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class sandwichHelper extends AbstractExtension
{

    /**
     * @var InflectorFactory
     */
    private $inflector;

    public function __construct()
    {

        $this->inflector = InflectorFactory::create()->build();


    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('displayManyMany', [$this, 'displayManyMany']),
            new TwigFilter('isObject', [$this, 'isObject']),
        ];
    }

    public function displayManyMany($manyManyArray, $name, $separator = ', '): string
    {
        $valuesArray = [];
        foreach ($manyManyArray as $item) {

            $valuesArray[] = $item->{'get'.$this->inflector->camelize($name)}();
        }
        return implode($separator,$valuesArray);
    }

    public function isObject($object)
    {
        return is_object($object);
    }

}