<?php

namespace App\Controller\admin;

use App\Entity\Meal;
use App\Entity\MealToken;
use App\Form\MealType;
use App\Repository\MealRepository;
use App\Services\MealService;
use App\Services\OpenWeatherApiService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/meal")
 * @IsGranted("ROLE_ADMIN_MEALS");
 */
class MealController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var MealService
     */
    private $mealService;

    public function __construct(MealService $mealService, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->mealService = $mealService;
    }

    /**
     * @Route("/", name="meal_index", methods={"GET"})
     * @param MealRepository $mealRepository
     * @param OpenWeatherApiService $openWeatherApiService
     * @return Response
     */
    public function index(MealRepository $mealRepository, OpenWeatherApiService $openWeatherApiService): Response
    {
        $admin = $this->getUser();
        $currentMeal = $this->em->getRepository(Meal::class)->CurrentMealByAdmin($admin);
        $pastMeals = $this->em->getRepository(Meal::class)->findByAdmin($admin);

        return $this->render('admin/meal/index.html.twig', [
            'currentMeal' => $currentMeal,
            'pastMeals' => $pastMeals,
        ]);
    }

    /**
     * @Route("/new", name="meal_new", methods={"GET","POST"})
     */
    public function new(): Response
    {

        $this->mealService->open();

        return $this->redirectToRoute('meal_index');
    }

    /**
     * @Route("/{id}", name="meal_show", methods={"GET"})
     * @param Meal $meal
     * @return Response
     */
    public function show(Meal $meal): Response
    {
        return $this->render('admin/meal/show.html.twig', [
            'meal' => $meal,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="meal_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Meal $meal
     * @return Response
     */
    public function edit(Request $request, Meal $meal): Response
    {
        $form = $this->createForm(MealType::class, $meal);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('meal_index');
        }

        return $this->render('admin/meal/edit.html.twig', [
            'meal' => $meal,
            'form' => $form->createView(),
        ]);
    }



    /**
     * @Route("/close/{id}", name="meal_close", methods={"GET"})
     * @param Meal $meal
     * @return Response
     */
    public function close(Meal $meal): Response
    {
        $this->mealService->close($meal);

        return $this->redirectToRoute('meal_index');
    }
}
