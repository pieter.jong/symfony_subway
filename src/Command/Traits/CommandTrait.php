<?php


namespace App\Command\Traits;


use Symfony\Component\Console\Style\SymfonyStyle;

trait CommandTrait
{
    /**
     * @var SymfonyStyle $io
     */
    protected $io;

    /**
     * @param string $message
     * @return CommandTrait
     */
    public function ioWriteSection(string $message): self
    {
        if ($this->io->isVerbose()) {
            $this->io->section($message);
        }
        return $this;
    }

    /**
     * @param string $message
     * @return CommandTrait
     */
    public function ioWriteLn(string $message): self
    {
        if ($this->io->isVerbose()) {
            $this->io->writeln($message);
        }
        return $this;
    }

    /**
     * @param string $message
     * @return CommandTrait
     */
    public function ioWriteTitle(string $message): self
    {
        if ($this->io->isVerbose()) {
            $this->io->title($message);
        }
        return $this;
    }
}
