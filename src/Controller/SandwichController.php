<?php

namespace App\Controller;

use App\Entity\Eater;
use App\Entity\Meal;
use App\Entity\Sandwich;
use App\Form\SandwichRateType;
use App\Form\SandwichType;
use App\Services\SandwichService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sandwich")
 */
class SandwichController extends AbstractController
{

    /**
     * @var SandwichService
     */
    private $sandwichService;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(SandwichService $sandwichService, EntityManagerInterface $em)
    {
        $this->sandwichService = $sandwichService;
        $this->em = $em;
    }

    /**
     * @Route("/{token}", name="sandwich_index", methods={"GET"})
     * @param string $token
     * @return Response
     */
    public function index(string $token): Response
    {
        $eater = $this->getEaterByToken($token);
        if (!$eater) {
            throw $this->createNotFoundException('This page does not exist');
        }
        $meal = $this->em->getRepository(Meal::class)->findByToken($token);
        $eatersSandwiches = $this->em->getRepository(Sandwich::class)->findByEaterAndMeal($eater, $meal);
        $combinedSandwiches = $this->sandwichService->combineSimultaneousSandwiches($eater);

//        dump($token);

        return $this->render('sandwich/index.html.twig', [
            'eater' => $eater,
            'meal' => $meal,
            'token' => $token,
            'eaterSandwiches' => $eatersSandwiches,
            'combinedSandwiches' => $combinedSandwiches
        ]);
    }

    /**
     * @Route("/new/{token}", name="sandwich_new", methods={"GET","POST"})
     * @param Request $request
     * @param string $token
     * @return Response
     */
    public function new(Request $request, string $token): Response
    {
        /** @var Eater $eater */
        $eater = $this->getEaterByToken($token);
        $admin = $eater->getUser();

        $sandwich = new Sandwich();
        $form = $this->createForm(SandwichType::class, $sandwich, ['admin' => $admin]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->sandwichService->store($request, $sandwich, $token, $form);
            return $this->redirectToRoute('sandwich_index', ['token' => $token]);
        }

        return $this->render('sandwich/new.html.twig', [
            'sandwich' => $sandwich,
            'form' => $form->createView(),
            'token' => $token
        ]);
    }


    /**
     * @Route("/{id}/edit/{token}", name="sandwich_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Sandwich $sandwich
     * @param string $token
     * @return Response
     */
    public function edit(Request $request, Sandwich $sandwich, string $token): Response
    {
        $form = $this->createForm(SandwichRateType::class, $sandwich);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sandwich_index', ['token' => $token]);
        }

        return $this->render('sandwich/edit.html.twig', [
            'sandwich' => $sandwich,
            'form' => $form->createView(),
            'token' => $token
        ]);
    }

    /**
     * @Route("/{id}/{token}", name="sandwich_delete", methods={"DELETE"})
     * @param Request $request
     * @param Sandwich $sandwich
     * @param string $token
     * @return Response
     */
    public function delete(Request $request, Sandwich $sandwich, string $token): Response
    {
        if ($this->isCsrfTokenValid('delete' . $sandwich->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($sandwich);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sandwich_index', ['token' => $token]);
    }

    private function getEaterByToken(string $token): ?Eater
    {
        return $this->em->getRepository(Eater::class)->getEaterIfValidToken($token);
    }

    /**
     * @Route("/rate/{sandwich}/{token}", name="sandwich_rate", methods={"GET", "POST"})
     * @param Request $request
     * @param Sandwich $sandwich
     * @param string $token
     * @return Response
     */
    public function rate(Request $request, Sandwich $sandwich, string $token): Response
    {
        /** @var Eater $eater */
        $eater = $this->getEaterByToken($token);

        $form = $this->createForm(SandwichRateType::class, $sandwich);
        $form->handleRequest($request);

        if ($form->isSubmitted() &&
            $form->isValid() &&
            $this->em->getRepository(Sandwich::class)->eaterBelongsToSandwich($eater, $sandwich)
        ) {
            $this->sandwichService->rate($request, $sandwich, $token, $form);
            return $this->redirectToRoute('sandwich_index', ['token' => $token]);
        }

        return $this->render('sandwich/rate.html.twig', [
            'sandwich' => $sandwich,
            'form' => $form->createView(),
            'token' => $token
        ]);
    }
}
