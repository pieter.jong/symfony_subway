<?php

namespace App\Repository;

use App\Entity\BreadSize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method BreadSize|null find($id, $lockMode = null, $lockVersion = null)
 * @method BreadSize|null findOneBy(array $criteria, array $orderBy = null)
 * @method BreadSize[]    findAll()
 * @method BreadSize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BreadSizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BreadSize::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('b.size', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
