<?php

namespace App\Entity;

use App\Repository\SandwitchRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SandwitchRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Sandwich extends BaseEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $baked;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", precision=3, nullable=true)
     */
    private $rating;

    /**
     * @ORM\ManyToOne(targetEntity=Bread::class, inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $bread;

    /**
     * @ORM\ManyToOne(targetEntity=BreadSize::class, inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity=Taste::class, inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $taste;

    /**
     * @ORM\ManyToMany(targetEntity=Extra::class, inversedBy="sandwiches")
     */
    private $extras;

    /**
     * @ORM\ManyToMany(targetEntity=Vegetables::class, inversedBy="sandwiches")
     */
    private $vegetables;

    /**
     * @ORM\ManyToOne(targetEntity=Sauce::class, inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $sauce;

    /**
     * @ORM\ManyToOne(targetEntity=Meal::class, inversedBy="sandwiches")
     */
    private $meal;

    /**
     * @ORM\ManyToOne(targetEntity=Eater::class, inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $eater;

    public function __construct()
    {
        $this->extras = new ArrayCollection();
        $this->vegetables = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBaked(): ?bool
    {
        return $this->baked;
    }

    public function setBaked(bool $baked): self
    {
        $this->baked = $baked;

        return $this;
    }

    public function getBread(): ?Bread
    {
        return $this->bread;
    }

    public function setBread(?Bread $bread): self
    {
        $this->bread = $bread;

        return $this;
    }

    public function getSize(): ?BreadSize
    {
        return $this->size;
    }

    public function setSize(?BreadSize $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getTaste(): ?Taste
    {
        return $this->taste;
    }

    public function setTaste(?Taste $taste): self
    {
        $this->taste = $taste;

        return $this;
    }

    /**
     * @return Collection|Extra[]
     */
    public function getExtras(): Collection
    {
        return $this->extras;
    }

    public function addExtra(Extra $extra): self
    {
        if (!$this->extras->contains($extra)) {
            $this->extras[] = $extra;
        }

        return $this;
    }

    public function removeExtra(Extra $extra): self
    {
        if ($this->extras->contains($extra)) {
            $this->extras->removeElement($extra);
        }

        return $this;
    }

    /**
     * @return Collection|Vegetables[]
     */
    public function getVegetables(): Collection
    {
        return $this->vegetables;
    }

    public function addVegetable(Vegetables $vegetable): self
    {
        if (!$this->vegetables->contains($vegetable)) {
            $this->vegetables[] = $vegetable;
        }

        return $this;
    }

    public function removeVegetable(Vegetables $vegetable): self
    {
        if ($this->vegetables->contains($vegetable)) {
            $this->vegetables->removeElement($vegetable);
        }

        return $this;
    }

    public function getSauce(): ?Sauce
    {
        return $this->sauce;
    }

    public function setSauce(?Sauce $sauce): self
    {
        $this->sauce = $sauce;

        return $this;
    }

    public function getMeal(): ?Meal
    {
        return $this->meal;
    }

    public function setMeal(?Meal $meal): self
    {
        $this->meal = $meal;

        return $this;
    }

    public function getEater(): ?Eater
    {
        return $this->eater;
    }

    public function setEater(?Eater $eater): self
    {
        $this->eater = $eater;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getRating(): ?float
    {
        return $this->rating;
    }

    /**
     * @param float|null $rating
     */
    public function setRating(?float $rating): void
    {
        $this->rating = $rating;
    }
}
