<?php

namespace App\Controller\admin;

use App\Entity\Bread;
use App\Entity\BreadSize;
use App\Entity\Extra;
use App\Entity\Sauce;
use App\Entity\Taste;
use App\Entity\Vegetables;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SandwichSettingsController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/sandwichsettings", name="sandwich_settings")
     * @IsGranted("ROLE_ADMIN_SANDWICH_SETTINGS");
     */
    public function index()
    {
        $admin = $this->getUser();

        $breads = $this->em->getRepository(Bread::class)->findByAdmin($admin);
        $breadSizes = $this->em->getRepository(BreadSize::class)->findByAdmin($admin);
        $extras = $this->em->getRepository(Extra::class)->findByAdmin($admin);
        $sauces = $this->em->getRepository(Sauce::class)->findByAdmin($admin);
        $tastes = $this->em->getRepository(Taste::class)->findByAdmin($admin);
        $vegetables = $this->em->getRepository(Vegetables::class)->findByAdmin($admin);

        return $this->render('admin/sandwich_settings/index.html.twig', [
            'breads' => $breads,
            'breadSizes' => $breadSizes,
            'extras' => $extras,
            'sauces' => $sauces,
            'tastes' => $tastes,
            'vegetables' => $vegetables,
        ]);
    }
}
