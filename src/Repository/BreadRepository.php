<?php

namespace App\Repository;

use App\Entity\Bread;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Bread|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bread|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bread[]    findAll()
 * @method Bread[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BreadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Bread::class);
    }

    /**
     * @param User $admin
     * @return int|mixed|string
     */
    public function findByAdmin(User $admin)
    {

        return $this->createQueryBuilder('b')
            ->andWhere('b.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('b.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
