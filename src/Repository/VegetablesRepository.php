<?php

namespace App\Repository;

use App\Entity\Vegetables;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Vegetables|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vegetables|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vegetables[]    findAll()
 * @method Vegetables[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VegetablesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vegetables::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('v.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
