<?php

namespace App\Controller\admin;

use App\Entity\Sauce;
use App\Form\SauceType;
use App\Services\SauceService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sauce")
 * @IsGranted("ROLE_ADMIN_SAUCE");
 */
class SauceController extends AbstractController
{
    /**
     * @var SauceService
     */
    private $sauceService;

    public function __construct(SauceService $sauceService)
    {
        $this->sauceService = $sauceService;
    }

    /**
     * @Route("/new", name="sauce_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $sauce = new Sauce();
        $form = $this->createForm(SauceType::class, $sauce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->sauceService->store($request, $sauce, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/sauce/new.html.twig', [
            'sauce' => $sauce,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sauce_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Sauce $sauce
     * @return Response
     */
    public function edit(Request $request, Sauce $sauce): Response
    {
        $form = $this->createForm(SauceType::class, $sauce);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->sauceService->update($request, $sauce, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/sauce/edit.html.twig', [
            'sauce' => $sauce,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sauce_delete", methods={"DELETE"})
     * @param Request $request
     * @param Sauce $sauce
     * @return Response
     */
    public function delete(Request $request, Sauce $sauce): Response
    {
        $this->sauceService->delete($request, $sauce);

        return $this->redirectToRoute('sandwich_settings');
    }
}
