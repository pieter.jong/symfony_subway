<?php

namespace App\Repository;

use App\Entity\Sauce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Sauce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sauce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sauce[]    findAll()
 * @method Sauce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SauceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sauce::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('s.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
