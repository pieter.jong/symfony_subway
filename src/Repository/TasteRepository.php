<?php

namespace App\Repository;

use App\Entity\Taste;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Taste|null find($id, $lockMode = null, $lockVersion = null)
 * @method Taste|null findOneBy(array $criteria, array $orderBy = null)
 * @method Taste[]    findAll()
 * @method Taste[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TasteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Taste::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
