<?php


namespace App\Services;

use App\Entity\Weather;
use App\Entity\WeatherForecast;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Scalar\String_;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class OpenWeatherApiService
{

    /**
     * @var string
     */
    private $apiKey;
    /**
     * @var string
     */
    private $units;
    /**
     * @var string
     */
    private $lang;

    public function __construct()
    {
        $this->apiKey = $_ENV['OPEN_WEATHER_KEY'];
        $this->units = 'metric';
        $this->lang = 'nl';
    }


    public function getCurrentWeatherForCity(string $city, ?string $lang = null): array
    {

        if(!$lang){
            $lang = $this->lang;
        }

        return $this->fetchData(sprintf(
                'api.openweathermap.org/data/2.5/weather?q=%s&appid=%s&units=%s&lang=%s',
                $city,
                $this->apiKey,
                $this->units,
                $lang
            )
        );
    }

    /**
     * @param $apiUrl
     *
     * @return mixed
     */
    private function fetchData($apiUrl)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = json_decode(curl_exec($ch), true);
        curl_close($ch);

        return $output;
    }
}