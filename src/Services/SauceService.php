<?php


namespace App\Services;


use App\Entity\Sauce;
use App\RequestValidators\StoreSauce;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class SauceService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var Security
     */
    private $security;
    /**
     * @var Sauce
     */
    private $storeSauce;


    public function __construct(StoreSauce $storeSauce, EntityManagerInterface $em, Security $security)
    {

        $this->storeSauce = $storeSauce;
        $this->em = $em;

        $this->security = $security;
    }

    /**
     * @param Request $request
     * @param Sauce $sauce
     * @param FormInterface|null $form
     * @return bool|ConstraintViolationListInterface
     */
    public function store(Request $request, Sauce $sauce, ?FormInterface $form = null)
    {
        $errors = $this->storeSauce->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $sauce->setUser($this->security->getUser());
        $this->em->persist($sauce);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Sauce $sauce
     * @param FormInterface $form
     * @return bool|ConstraintViolationListInterface
     */
    public function update(Request $request, Sauce $sauce, FormInterface $form)
    {
        $errors = $this->storeSauce->validateObjectByRequest($request, $form);
        if (count($errors)) {
            return $errors;
        }
        $this->em->persist($sauce);
        $this->em->flush();

        return true;
    }

    /**
     * @param Request $request
     * @param Sauce $sauce
     */
    public function delete(Request $request, Sauce $sauce): void
    {
        $this->em->remove($sauce);
        $this->em->flush();
    }
}