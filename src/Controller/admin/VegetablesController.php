<?php

namespace App\Controller\admin;

use App\Entity\Vegetables;
use App\Form\VegetablesType;
use App\Services\VegetablesService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/vegetables")
 * @IsGranted("ROLE_ADMIN_VEGETABLES");
 */
class VegetablesController extends AbstractController
{

    /**
     * @var VegetablesService
     */
    private $vegetablesService;

    public function __construct(VegetablesService $vegetablesService)
    {
        $this->vegetablesService = $vegetablesService;
    }

    /**
     * @Route("/new", name="vegetables_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $vegetable = new Vegetables();
        $form = $this->createForm(VegetablesType::class, $vegetable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->vegetablesService->store($request, $vegetable, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/vegetables/new.html.twig', [
            'vegetable' => $vegetable,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="vegetables_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Vegetables $vegetable
     * @return Response
     */
    public function edit(Request $request, Vegetables $vegetable): Response
    {
        $form = $this->createForm(VegetablesType::class, $vegetable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->vegetablesService->update($request, $vegetable, $form) === true) {
            return $this->redirectToRoute('sandwich_settings');
        }

        return $this->render('admin/vegetables/edit.html.twig', [
            'vegetable' => $vegetable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="vegetables_delete", methods={"DELETE"})
     * @param Request $request
     * @param Vegetables $vegetable
     * @return Response
     */
    public function delete(Request $request, Vegetables $vegetable): Response
    {
        $this->vegetablesService->delete($request, $vegetable);

        return $this->redirectToRoute('sandwich_settings');
    }
}
