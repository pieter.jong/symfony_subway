<?php

namespace App\Command;

use App\Command\Traits\CommandTrait;
use App\Entity\Weather;
use App\Services\OpenWeatherApiService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class getWeatherCommand extends Command
{

    use CommandTrait;

    private $openWeatherApiService;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(OpenWeatherApiService $openWeatherApiService, EntityManagerInterface $em)
    {
        $this->openWeatherApiService = $openWeatherApiService;
        parent::__construct();
        $this->em = $em;
    }

    protected function configure(): void
    {
        $this
            ->setName('command:getWeather')
            ->setDescription('Recieves weather and stores in db')
            ->setHelp('This command creates a weather record in your database based on your location')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ioWriteTitle('Command command:getWeather started');
        $location = 'Leeuwarden';

        $weather = $this->openWeatherApiService->getCurrentWeatherForCity($location);
        $this->ioWriteLn( sprintf('Received weather for location %s', $location));
        $this->weatherWrite($weather);
        $this->ioWriteLn( sprintf('Weather for location %s stored in db', $location));

        $this->ioWriteTitle('Command finished');

        return 1;
    }

    /**
     * @param array $weather
     */
    private function weatherWrite(array $weather): void
    {
        $newWeather = new Weather();

        $newWeather->setCity($weather['name']);
        $newWeather->setTemp($weather['main']['temp']);
        $newWeather->setTempMin($weather['main']['temp_min']);
        $newWeather->setTempMax($weather['main']['temp_max']);
        $newWeather->setHumidity($weather['main']['humidity']);
        $newWeather->setFeelsLike($weather['main']['feels_like']);

        $this->em->persist($newWeather);
        $this->em->flush();
    }
}
