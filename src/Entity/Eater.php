<?php

namespace App\Entity;

use App\Repository\EaterRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=EaterRepository::class)
 * @UniqueEntity("email")
 */
class Eater
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="eaters")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=MealToken::class, mappedBy="eater", orphanRemoval=true)
     */
    private $mealTokens;

    /**
     * @ORM\ManyToMany(targetEntity=Meal::class, mappedBy="eaters")
     */
    private $meals;

    /**
     * @ORM\OneToMany(targetEntity=Sandwich::class, mappedBy="eater", orphanRemoval=true)
     */
    private $sandwiches;

    public function __construct()
    {
        $this->mealTokens = new ArrayCollection();
        $this->meals = new ArrayCollection();
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|MealToken[]
     */
    public function getMealTokens(): Collection
    {
        return $this->mealTokens;
    }

    public function addMealToken(MealToken $mealToken): self
    {
        if (!$this->mealTokens->contains($mealToken)) {
            $this->mealTokens[] = $mealToken;
            $mealToken->setEater($this);
        }

        return $this;
    }

    public function removeMealToken(MealToken $mealToken): self
    {
        if ($this->mealTokens->contains($mealToken)) {
            $this->mealTokens->removeElement($mealToken);
            // set the owning side to null (unless already changed)
            if ($mealToken->getEater() === $this) {
                $mealToken->setEater(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Meal[]
     */
    public function getMeals(): Collection
    {
        return $this->meals;
    }

    public function addMeal(Meal $meal): self
    {
        if (!$this->meals->contains($meal)) {
            $this->meals[] = $meal;
            $meal->addEater($this);
        }

        return $this;
    }

    public function removeMeal(Meal $meal): self
    {
        if ($this->meals->contains($meal)) {
            $this->meals->removeElement($meal);
            $meal->removeEater($this);
        }

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->setEater($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            // set the owning side to null (unless already changed)
            if ($sandwich->getEater() === $this) {
                $sandwich->setEater(null);
            }
        }

        return $this;
    }
}
