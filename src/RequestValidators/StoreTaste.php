<?php


namespace App\RequestValidators;
use Symfony\Component\Validator\Constraints as Assert;

class StoreTaste extends CustomValidator
{
    /**
     * @Assert\Length(
     *     min = 3,
     *     max = 10000,
     *)
     */
    public $name;

}