<?php

namespace App\Controller;

use App\Entity\Eater;
use App\Entity\Meal;
use App\Entity\Sandwich;
use App\Form\SandwichRateType;
use App\Services\SandwichService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{


    /**
     * @Route("/", methods={"GET"})
     * @return Response
     */
    public function index()
    {
        return $this->redirectToRoute('meal_index');
    }
}
