<?php

namespace App\Form;

use App\Entity\Bread;
use App\Entity\BreadSize;
use App\Entity\Extra;
use App\Entity\Sandwich;
use App\Entity\Sauce;
use App\Entity\Taste;
use App\Entity\Vegetables;
use App\Repository\BreadRepository;
use App\Repository\BreadSizeRepository;
use App\Repository\ExtraRepository;
use App\Repository\SauceRepository;
use App\Repository\TasteRepository;
use App\Repository\VegetablesRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SandwichRateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('rating', NumberType::class,[
            'scale' => 3,
            'label' => 'rate (1 to 10)'
        ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sandwich::class,
        ]);
    }
}
