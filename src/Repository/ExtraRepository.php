<?php

namespace App\Repository;

use App\Entity\Extra;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Extra|null find($id, $lockMode = null, $lockVersion = null)
 * @method Extra|null findOneBy(array $criteria, array $orderBy = null)
 * @method Extra[]    findAll()
 * @method Extra[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Extra::class);
    }

    /**
     * @param UserInterface|null $admin
     * @return int|mixed|string
     */
    public function findByAdmin(?UserInterface $admin)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.user = :admin')
            ->setParameter('admin', $admin)
            ->addOrderBy('e.name', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
