<?php

namespace App\Entity;

use App\Repository\VegetablesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VegetablesRepository::class)
 */
class Vegetables
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Sandwich::class, mappedBy="vegetables")
     */
    private $sandwiches;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="vegetables")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct()
    {
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->addVegetable($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            $sandwich->removeVegetable($this);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
