<?php

namespace App\Controller\admin;

use App\Entity\Eater;
use App\Form\EaterType;
use App\Repository\EaterRepository;
use App\Services\EaterService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/eater")
 * @IsGranted("ROLE_ADMIN_EATER");
 */
class EaterController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var EaterService
     */
    private $eaterService;

    public function __construct(EntityManagerInterface $em, EaterService $eaterService)
    {
        $this->em = $em;
        $this->eaterService = $eaterService;
    }

    /**
     * @Route("/", name="eater_index", methods={"GET"})
     */
    public function index(): Response
    {
        $admin = $this->getUser();

        $eaters = $this->em->getRepository(Eater::class)->findByAdmin($admin);
        return $this->render('admin/eater/index.html.twig', [
            'eaters' => $eaters,
        ]);
    }

    /**
     * @Route("/new", name="eater_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $eater = new Eater();
        $form = $this->createForm(EaterType::class, $eater);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $this->eaterService->store($request, $eater, $form) === true) {
            return $this->redirectToRoute('eater_index');
        }

        return $this->render('admin/eater/new.html.twig', [
            'eater' => $eater,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="eater_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Eater $eater): Response
    {
        $form = $this->createForm(EaterType::class, $eater);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('eater_index');
        }

        return $this->render('admin/eater/edit.html.twig', [
            'eater' => $eater,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="eater_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Eater $eater): Response
    {
        if ($this->isCsrfTokenValid('delete'.$eater->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($eater);
            $entityManager->flush();
        }

        return $this->redirectToRoute('eater_index');
    }
}
