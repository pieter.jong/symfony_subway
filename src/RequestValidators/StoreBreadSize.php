<?php


namespace App\RequestValidators;
use Symfony\Component\Validator\Constraints as Assert;

class StoreBreadSize extends CustomValidator
{
    /**
     * @Assert\Type("string")
     */
    public $size;

}